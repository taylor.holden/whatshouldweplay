const express = require('express')
const R = require('ramda')

const { fetchFriendsListForSteamUser, fetchPlayerSummaryForSteamUsers } = require('../services/steamApi')
const errors = require('../constants/errors')

const router = express.Router()

router.get('/', handleGetFriends)


async function handleGetFriends(req, res) {
  try {
    const friendIds = await fetchFriendIds(req.steamUserId)

    if (friendIds.length === 0) {
      res.status(400)
      res.json({ error: errors.FRIENDLIST_PRIVATE })
      return
    }

    const promises = chunkArray(friendIds).reduce(
      (acc, chunkOfFriends) => {
        acc.push(fetchPlayerSummaryForSteamUsers(chunkOfFriends))
        return acc
      },
      []
    )

    const responses = await Promise.all(promises)

    res.json(
      flatten(responses)
        .map(mapToFriendData)
        .sort(
          (a, b) => a.username.toLowerCase().localeCompare(b.username.toLowerCase())
        )
    )
  } catch (err) {
    res.status(500)
    res.json({ error: err })
  }
}

const fetchFriendIds = async steamUserId => {
  try {
    const response = await fetchFriendsListForSteamUser(steamUserId)

    return response.map(friendMeta => friendMeta.steamid)
  } catch (err) {
    if (err === errors.FRIENDLIST_PRIVATE) {
      return []
    } else {
      throw err
    }
  }
}

const chunkArray = (arr, chunkSize = 100) => {
  const chunked = []
  for(let i = 0; i < arr.length; i += chunkSize) {
    chunked.push(arr.slice(i, i+chunkSize))
  }

  return chunked
}

const flatten = R.reduce(R.concat, [])

const mapToFriendData = ({ steamid, personaname, avatarmedium }) => ({
  steamid,
  username: personaname,
  avatarUrl: avatarmedium
})

module.exports = {
  router,
  handleGetFriends
}