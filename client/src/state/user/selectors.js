import { createSelector } from 'reselect'
import { intersection } from 'ramda'

export const selectUser = state => state.user
export const selectUserId = createSelector(selectUser, user => user.userId)
