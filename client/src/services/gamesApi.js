import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { prepareDefaultHeaders } from './apiHelpers'

const gamesApi = createApi({
  reducerPath: 'gamesApi',
  baseQuery: fetchBaseQuery({
    baseUrl: '/api',
    prepareHeaders: prepareDefaultHeaders()
  }),
  endpoints: (builder) => ({
    getGamesForUser: builder.query({
      query: steamUserId => `users/${steamUserId}/games`
    })
  })
})

export default gamesApi
