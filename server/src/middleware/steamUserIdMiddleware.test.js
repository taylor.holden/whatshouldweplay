const {
  steamUserIdMiddleware,
  getUserId
} = require('./steamUserIdMiddleware')

describe('#steamUserApiMiddleware', () => {
  describe('#getUserId', () => {
    it('should get the header \'Steam-User-ID\' from the req object', () => {
      const req = {
        header: jest.fn(header => header)
      }

      const result = getUserId(req)

      expect(result).toBe('Steam-User-ID')
      expect(req.header).toHaveBeenCalledTimes(1)
      expect(req.header).toHaveBeenCalledWith('Steam-User-ID')
    })

    it('should also try to get \'steam-user-id\' header if normal one is missing', () => {
      const req = {
        header: jest.fn(header => {
          if (header !== 'Steam-User-ID') {
            return header
          }
        })
      }

      const result = getUserId(req)


      expect(result).toBe('steam-user-id')
      expect(req.header).toHaveBeenCalledTimes(2)
      expect(req.header).toHaveBeenCalledWith('Steam-User-ID')
      expect(req.header).toHaveBeenCalledWith('steam-user-id')
    })
  })

  it('should error if no steam user id header is present', () => {
    const req = {
      header: jest.fn()
    }
    const res = {
      status: jest.fn(),
      json: jest.fn()
    }
    const next = jest.fn()

    steamUserIdMiddleware(req, res, next)

    expect(res.status).toHaveBeenCalledTimes(1)
    expect(res.status).toHaveBeenCalledWith(400)
    expect(res.json).toHaveBeenCalledTimes(1)
    expect(res.json).toHaveBeenCalledWith({ error: 'No Steam User Id provided' })
    expect(next).not.toHaveBeenCalled()
  })

  it('should set the steamUserId prop on the req if header is present and then invoke next()', () => {
    const req = {
      header: jest.fn(header => header)
    }
    const res = {
      status: jest.fn(),
      json: jest.fn()
    }
    const next = jest.fn()
    const expectedSteamUserId = 'me id'

    req.header.mockReturnValue(expectedSteamUserId)

    steamUserIdMiddleware(req, res, next)

    expect(res.status).not.toHaveBeenCalled()
    expect(res.json).not.toHaveBeenCalled()
    expect(req.steamUserId).toEqual(expectedSteamUserId)
    expect(next).toHaveBeenCalled()
  })
})