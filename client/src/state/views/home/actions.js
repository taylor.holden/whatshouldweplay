import { createAction } from '@reduxjs/toolkit'

export const setFriendSelectorOpen = createAction('SET_FRIEND_SELECTOR_OPEN')
export const setFriendSelected = createAction('SET_FRIEND_SELECTED')

export const setGameSelectorOpen = createAction('SET_GAME_SELECTOR_OPEN')