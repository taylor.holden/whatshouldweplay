import { createAction } from '@reduxjs/toolkit'

export const setUser = createAction('SET_USER')