import { is } from 'ramda'

const USER_ID_HEADER = 'Steam-User-ID'

export const prepareDefaultHeaders = (prepareOtherHeaders) => (headers, { getState }) => {
  const userId = getState().user.userId

  // If we have a token set in state, let's assume that we should be passing it.
  if (userId) {
    headers.set(USER_ID_HEADER, userId)
  }

  if (is(Function, prepareOtherHeaders)) {
    prepareOtherHeaders(headers, { getState })
  }

  return headers
}