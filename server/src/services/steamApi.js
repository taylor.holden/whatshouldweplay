const { getJson } = require('../utils')
const errors = require('../constants/errors')

const fetchFriendsListForSteamUser = async steamUserId => {
  const response = await getJson(
    `http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=${process.env.API_KEY}&steamid=${steamUserId}&relationship=friend`
  )

  if (response.body.friendslist) {
    return response.body.friendslist.friends
  }
  throw errors.FRIENDLIST_PRIVATE
}

const fetchPlayerSummaryForSteamUsers = async steamUserIds => {
  const commaSeparatedIds = steamUserIds.join(',')
  const response = await getJson(
    `http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=${process.env.API_KEY}&steamids=${commaSeparatedIds}`
  )

  return response.body.response.players
}

const fetchGamesForSteamUser = async steamUserId => {
  const response = await getJson(
    `http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${process.env.API_KEY}&steamid=${steamUserId}&include_appinfo=true`
  )

  return {
    steamUserId,
    games: (response.body.response.games || []),
    gameCount: (response.body.response.game_count || [])
  }
}

module.exports = {
  fetchGamesForSteamUser,
  fetchPlayerSummaryForSteamUsers,
  fetchFriendsListForSteamUser
}