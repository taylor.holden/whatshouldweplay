import React from 'react'
import { darken } from 'polished'
import styled from 'styled-components'

const Bar = ({ className, children, onClick }) => {
  return (
    <div className={className} onClick={onClick}>
      {children}
    </div>
  )
}

export default styled(Bar)`
  background-color: ${({ theme }) => darken(0.1, theme.background.color.default)};
  box-shadow: 0 0 4px ${({ theme }) => darken(0.1, theme.border.color.default)};
  display: flex;
  flex-direction: row;
  flex-grow: 0;
  flex-wrap: wrap;
  padding: ${({ theme }) => theme.spacing.unit}px;
  position: relative;
`