import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'

import { gamesApi, userApi } from 'wswpServices'
import { selectUserId } from 'wswpState'

import FriendSelector from './FriendSelector'
import GameSelector from './GameSelector'

function Home({ className }) {
  const dispatch = useDispatch()
  const steamUserId = useSelector(selectUserId)

  if (steamUserId && steamUserId.length) {
    userApi.useGetCurrentUserQuery()
  }

  useEffect(() => {
    dispatch(gamesApi.endpoints.getGamesForUser.initiate(steamUserId, { track: false }))
  }, [steamUserId])

  return (
    <>
      <FriendSelector />
      <GameSelector />
    </>
  )
}

export default styled(Home)`
`
