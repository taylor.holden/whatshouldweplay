import Home from './Home/index'
import Login from './Login'
import NavBar from './NavBar/index'

export {
  Home,
  Login,
  NavBar
}
