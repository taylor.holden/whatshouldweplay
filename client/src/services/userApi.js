import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { prepareDefaultHeaders } from './apiHelpers'

const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: fetchBaseQuery({
    baseUrl: '/api/users',
    prepareHeaders: prepareDefaultHeaders()
  }),
  endpoints: (builder) => ({
    getCurrentUser: builder.query({
      query: () => ''
    })
  })
})

export default userApi
