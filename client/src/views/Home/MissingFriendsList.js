import React from 'react'
import styled from 'styled-components'

import { Button, Text } from 'wswpComponents'

const MissingFriendsList = ({ className }) => {
  const refreshClick = () => {
    location.reload()
  }

  return (
    <Container>
      <div className={className}>
        <Text centered>
          We were not able to retrieve your friends list.
        </Text>
        <Text variant="caption" centered>
           Please confirm that your Friends List privacy setting is set to "Public" and try to <Button onClick={refreshClick}>Refresh</Button>
        </Text>
      </div>
    </Container>
  )
}

const Container = styled.div`
  align-items: center;
  display: flex;
  flex-grow: 1;
  height: 100%;
  justify-content: center;
  width: 100%;
 `

export default styled(MissingFriendsList)`
  height: 300px;
  width: 400px;
  background-color: ${({ theme }) => theme.background.color.highlight};
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  box-shadow:
        0 1px 0 rgba(0,0,0,.08),
        inset 0 1px 2px rgba(255,255,255,.67),
        inset 0 -1px 0 rgba(0,0,0,.14);
`