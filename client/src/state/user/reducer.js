import { createReducer } from '@reduxjs/toolkit'

import { setUser } from './actions'

const userReducer = createReducer(
  {
    userId: (localStorage.getItem('steamUserId') || '')
  },
  builder => {
    builder.addCase(setUser, (state, action) => {
      state.userId = action.payload
      localStorage.setItem('steamUserId', action.payload)
    })
  }
)

export default userReducer
