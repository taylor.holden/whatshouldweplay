import React from 'react'
import styled from 'styled-components'
import {Info, CheckCircle} from '@styled-icons/material'

import { gamesApi } from 'wswpServices'
import { Text, Tooltip } from 'wswpComponents'

const SelectedFriend = ({ className, friend, onClick }) => {
  const foo = gamesApi.useGetGamesForUserQuery(friend.steamid)
  const mode = foo?.data?.games?.length < 1 ? 'warning' : 'selected'
  const warningMessage = ' This friend\'s privacy settings prevent fetching their games.'
  return (
    <FriendContainer className={className} onClick={onClick} mode={mode}>
      <img height="32" width="32" src={friend.avatarUrl}/>
      <Text>{friend.username} {mode === 'warning' ?
        <Tooltip messageText={warningMessage}><WarningIcon size="1em"/></Tooltip> :
        <SuccessIcon size="1em"/>}
      </Text>
    </FriendContainer>
  )
}
const UnselectedFriend = ({ className, friend, onClick }) => {
  return (
    <FriendContainer className={className} onClick={onClick} mode={'default'}>
      <img height="32" width="32" src={friend.avatarUrl}/> <Text>{friend.username}</Text>
    </FriendContainer>
  )
}
const Friend = ({ selected, ...otherProps }) => {
  if (selected) {
    return <SelectedFriend selected={selected} {...otherProps} />
  }
  return <UnselectedFriend selected={selected} {...otherProps} />
}
const getBackgroundColor = (mode) => {
  if (mode === 'warning') {
    return 'orange'
  } else if (mode === 'selected') {
    return 'green'
  } else {
    return 'inherit'
  }
}

const WarningIcon = styled(Info)`
  color: red;
`
const SuccessIcon = styled(CheckCircle)`
  color: lawngreen;
`

const FriendContainer = styled.div`
  border: 1px solid ${({theme}) => theme.border.color.default};
  background-color: ${({theme, mode}) =>getBackgroundColor(mode)};
  padding: ${({theme}) => theme.spacing.unit}px;
`



export default Friend