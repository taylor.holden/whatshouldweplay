const {
  handleGetUser,
  handleGetGames,
  router
} = require('./usersRouter')
const errors = require('../constants/errors')
const { fetchPlayerSummaryForSteamUsers, fetchGamesForSteamUser } = require('../services/steamApi')

jest.mock('../services/steamApi', () => ({
  fetchPlayerSummaryForSteamUsers: jest.fn(),
  fetchGamesForSteamUser: jest.fn()
}))

describe('#usersRouter', () => {
  afterEach(() => {
    fetchPlayerSummaryForSteamUsers.mockClear()
    fetchGamesForSteamUser.mockClear()
  })

  describe('#handleGetUser', () => {
    it('should request the player summary for the current user', async () => {
      const req = {
        steamUserId: 'steamUserApi'
      }
      const res = {
        json: jest.fn()
      }
      const userResponse = ['taylor']

      const expected = userResponse[0]

      fetchPlayerSummaryForSteamUsers.mockReturnValueOnce(Promise.resolve(userResponse))

      await handleGetUser(req, res)

      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledWith([req.steamUserId])
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith(expected)
    })

    it('should handle any errors well and forward them to user', async () => {
      const req = {
        steamUserId: 'steamUserApi'
      }
      const res = {
        status: jest.fn(),
        json: jest.fn()
      }
      const userError = 'error of some kind'

      fetchPlayerSummaryForSteamUsers.mockReturnValueOnce(Promise.reject(userError))

      await handleGetUser(req, res)

      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledWith([req.steamUserId])
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(500)
      expect(res.json).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith({  error: userError })
    })

    it('should return a 400 for the FRIENDLIST_PRIVATE error', async () => {
      const req = {
        steamUserId: 'steamUserApi'
      }
      const res = {
        status: jest.fn(),
        json: jest.fn()
      }
      const userError = errors.FRIENDLIST_PRIVATE

      fetchPlayerSummaryForSteamUsers.mockReturnValueOnce(Promise.reject(userError))

      await handleGetUser(req, res)

      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledWith([req.steamUserId])
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(400)
      expect(res.json).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith({  error: userError })
    })
  })

  describe('#handleGetGames', () => {
    it('should fetch the games for the requested user', async () => {
      const req = {
        params: {
          userId: 'foo'
        }
      }
      const res = {
        json: jest.fn(),
        status: jest.fn()
      }
      const gameListResponse = { games: [{ appid: 1, img_logo_url: 'url', name: 'kotor', droppedProp: 'foo' }] }
      const expected = { games: [{ appid: 1, imgUrl: 'url', name: 'kotor' }]}

      fetchGamesForSteamUser.mockReturnValueOnce(Promise.resolve(gameListResponse))

      await handleGetGames(req, res)

      expect(fetchGamesForSteamUser).toBeCalledTimes(1)
      expect(fetchGamesForSteamUser).toHaveBeenCalledWith(req.params.userId)
      expect(res.json).toHaveBeenCalledWith(expected)
    })

    it('should handle any errors well and forward them to user', async () => {
      const req = {
        params: {
          userId: 'foo'
        }
      }
      const res = {
        json: jest.fn(),
        status: jest.fn()
      }
      const gameListError = 'error of some kind'
      const expected = { error: gameListError }

      fetchGamesForSteamUser.mockReturnValueOnce(Promise.reject(gameListError))

      await handleGetGames(req, res)

      expect(fetchGamesForSteamUser).toBeCalledTimes(1)
      expect(fetchGamesForSteamUser).toHaveBeenCalledWith(req.params.userId)
      expect(res.status).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(500)
      expect(res.json).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith(expected)
    })
  })
})
