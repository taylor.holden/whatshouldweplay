import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import styled, { ThemeProvider } from 'styled-components'

import store from 'wswpState/store'

import AppRouter from './AppRouter'
import theme from './styles/theme'
import registerServiceWorker from './registerServiceWorker'

registerServiceWorker()

const AppWrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => theme.background.color.default};
  display: flex;
  flex-direction: column;
`

ReactDOM.render(
  (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <AppWrapper>
          <AppRouter />
        </AppWrapper>
      </Provider>
    </ThemeProvider>
  ),
  document.getElementById('root')
)
