module.exports = {
  bail: 1,
  collectCoverage: true,
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '<rootDir>(.*)\.index.js'
  ],
  setupFiles: ["<rootDir>/.jest/setEnvironmentVariables.js"],
  verbose: true
}