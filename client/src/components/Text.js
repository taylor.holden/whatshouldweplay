import React from 'react'
import styled from 'styled-components'

export default function({ children, variant, ...otherProps }) {
  switch(variant) {
    case 'caption':
      return <Caption {...otherProps}>{children}</Caption>
    case 'headline':
      return <Headline {...otherProps}>{children}</Headline>
    case 'text':
    default:
      return <Text {...otherProps}>{children}</Text>
  }
}

const getColor = ({ contrast, theme }) => contrast ? theme.text.color.contrast : theme.text.color.default

const Text = styled.span`
  color: ${getColor};
  text-align: ${({ centered }) => centered ? 'center' : 'left'};
`

const Headline = styled(Text)`
  font-size: 1.5em;
`

const Caption = styled(Text)`
  font-size: 0.6em;
`