import { lighten, darken } from 'polished'

const darkBlue = '#223d59'
const lightGrey = '#d5d5d5'
const hotPink = '#ff007f'
const white = '#ffffff'
const darkGrey = '#171717'

export default {
  background: {
    color: {
      default: darkBlue,
      contrast: lightGrey,
      highlight: hotPink
    }
  },
  border: {
    color: {
      default: darken(0.2, darkBlue)
    },
    size: {
      default: 1
    }
  },
  text: {
    color: {
      default: white,
      contrast: darkGrey,
      highlight: darkGrey,
      link: white
    },
    fontSize: {
      default: '1em',
      caption: '0.75em',
      headline: '2em'
    },
    fontWeight: {
      default: 600,
      caption: 300,
      headline: 900
    }
  },
  spacing: {
    unit: 8
  }
}