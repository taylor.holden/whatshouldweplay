import React from 'react'
import styled, { keyframes } from 'styled-components'
import { darken, lighten } from 'polished'

const LinearProgressIndicator = ({ width = '100%' }) => (
  <Track width={width}>
    <Indicator />
  </Track>
)

const Track = styled.div`
  background-color: ${({ theme }) => lighten(0.2, theme.background.color.default)};
  flex-basis: ${({ width }) => width};
  height: ${({ theme }) => theme.spacing.unit / 2}px;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  width: ${({ width }) => width};
`

const slideEndToEndAnimation = keyframes`
  0% { left: -40%; right: 0; }
  50% { left: 30%; right: 40%; }
  100% { left: 100%; }
`

const Indicator = styled.div`
  animation-name: ${slideEndToEndAnimation};
  animation-duration: 3s;
  animation-iteration-count: infinite;
  background-color: ${({ theme }) => darken(0.2, theme.background.color.default)};
  height: ${({ theme }) => theme.spacing.unit / 2}px;
  width: 40%;
  position: relative;
`

export default LinearProgressIndicator