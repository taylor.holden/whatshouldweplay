const http = require('http')

const getJson = url => new Promise((resolve, reject) => {
  http.get(
    url,
    res => {
      let body = ''

      res.on('data', function(chunk) {
        body += chunk
      })

      res.on('end', function() {
        res.body = JSON.parse(body)
        resolve(res)
      })
    }
  )
})

module.exports = {
  getJson
}