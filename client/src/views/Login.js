import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'

import { Button, Link, Text } from 'wswpComponents'
import { userApi } from 'wswpServices'
import { setUser, selectUserId } from 'wswpState'

function Login({ className }) {
  const history = useHistory()
  const dispatch = useDispatch()

  const [steamUserId, setSteamUserId] = useState('')
  const reduxSteamUserId = useSelector(selectUserId)
  const fetchedUser = useSelector(userApi.endpoints.getCurrentUser.select())

  const handleUpdate = useCallback(event => {
    setSteamUserId(event.target.value)
  }, [setSteamUserId])

  const handleSubmit = useCallback(() => {
    dispatch(setUser(steamUserId))
  }, [steamUserId])

  useEffect(() => {
    if (reduxSteamUserId && reduxSteamUserId.length) {
      dispatch(userApi.endpoints.getCurrentUser.initiate())
    }
  }, [reduxSteamUserId])

  useEffect(() => {
    console.log('current fetchedUser', fetchedUser)
    if (fetchedUser && fetchedUser.status === 'fulfilled') {
      history.push('/home')
    }
  }, [fetchedUser])

  return (
    <div className={className}>
      <Text variant="headline">What is your</Text>
      <Text variant="headline">Steam User ID?</Text>
      <Text variant="caption">You can find your ID <Link href="https://store.steampowered.com/account/" target="_blank">here</Link></Text>
      <div>
        <input id="userId" type="text" value={steamUserId} onChange={handleUpdate} /><Button onClick={handleSubmit}>Begin</Button>
      </div>
    </div>
  )
}

export default styled(Login)`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
