import friendsApi from './friendsApi'
import gamesApi from './gamesApi'
import userApi from './userApi'

export {
  friendsApi,
  gamesApi,
  userApi
}