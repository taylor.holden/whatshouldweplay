const cors = require('cors')
const path = require('path')
const express = require('express')
const { router } = require('./routers')

const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())

app.use('/', express.static(path.join(__dirname, '../../client/dist')))
app.use('/api', router)
app.get( '*', function( req, res ) {
  res.sendFile(path.resolve( __dirname, '../../client/dist/index.html'))
})

const PORT = process.env.PORT || 8080
app.listen(PORT)
console.log('Magic is happening on port ' + PORT)
