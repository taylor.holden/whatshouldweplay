const {
  handleGetFriends
} = require('./friendsRouter')
const errors = require('../constants/errors')
const { fetchPlayerSummaryForSteamUsers, fetchFriendsListForSteamUser } = require('../services/steamApi')

jest.mock('../services/steamApi', () => ({
  fetchFriendsListForSteamUser: jest.fn(),
  fetchPlayerSummaryForSteamUsers: jest.fn()
}))

describe('#friendsRouter', () => {
  afterEach(() => {
    fetchFriendsListForSteamUser.mockClear()
    fetchPlayerSummaryForSteamUsers.mockClear()
  })

  describe('#handleGetFriends', () => {
    it('should request the summaries for all of the friends of the current user', async () => {
      const req = {
        steamUserId: 'steamUserId'
      }
      const res = {
        json: jest.fn()
      }
      const friendsListResponse = [{ steamid: 1 }]
      const summariesResponse = [{ steamid: 1, personaname: 'taylor', avatarmedium: 'the last airbender' }]

      const expectedResponse = [{
        steamid: 1,
        username: 'taylor',
        avatarUrl: 'the last airbender'
      }]

      fetchFriendsListForSteamUser.mockReturnValueOnce(Promise.resolve(friendsListResponse))
      fetchPlayerSummaryForSteamUsers.mockReturnValueOnce(Promise.resolve(summariesResponse))

      await handleGetFriends(req, res)

      expect(fetchFriendsListForSteamUser).toHaveBeenCalledWith(req.steamUserId)
      expect(fetchFriendsListForSteamUser).toHaveBeenCalledTimes(1)
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledWith([1])
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith(expectedResponse)
    })

    it('should sort the friends by username', async () => {
      const req = {
        steamUserId: 'steamUserId'
      }
      const res = {
        json: jest.fn()
      }
      const friendsListResponse = [{ steamid: 1 }]
      const summariesResponse = [{
        steamid: 1,
        personaname: 'Taylor',
        avatarmedium: 'the last airbender'
      }, {
        steamid: 1,
        personaname: 'baylor',
        avatarmedium: 'the last airbender'
      }]

      const expectedResponse = [{
        steamid: 1,
        username: 'baylor',
        avatarUrl: 'the last airbender'
      }, {
        steamid: 1,
        username: 'Taylor',
        avatarUrl: 'the last airbender'
      }]

      fetchFriendsListForSteamUser.mockReturnValueOnce(Promise.resolve(friendsListResponse))
      fetchPlayerSummaryForSteamUsers.mockReturnValueOnce(Promise.resolve(summariesResponse))

      await handleGetFriends(req, res)

      expect(fetchFriendsListForSteamUser).toHaveBeenCalledWith(req.steamUserId)
      expect(fetchFriendsListForSteamUser).toHaveBeenCalledTimes(1)
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledWith([1])
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith(expectedResponse)
    })

    it('should error with a 400 if there are no friends (assumed private account)', async () => {
      const req = {
        steamUserId: 'steamUserId'
      }
      const res = {
        status: jest.fn(),
        json: jest.fn()
      }

      const expectedResponse = errors.FRIENDLIST_PRIVATE

      fetchFriendsListForSteamUser.mockReturnValueOnce(Promise.reject(expectedResponse))

      await handleGetFriends(req, res)

      expect(fetchFriendsListForSteamUser).toHaveBeenCalledWith(req.steamUserId)
      expect(fetchFriendsListForSteamUser).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(400)
      expect(res.json).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith({ error: expectedResponse })

      expect(fetchPlayerSummaryForSteamUsers).not.toHaveBeenCalled()
    })

    it('should handle other errors cleanly', async () => {
      const req = {
        steamUserId: 'steamUserId'
      }
      const res = {
        status: jest.fn(),
        json: jest.fn()
      }

      const expectedResponse = 'unhandled error'

      fetchFriendsListForSteamUser.mockReturnValueOnce(Promise.reject(expectedResponse))

      await handleGetFriends(req, res)

      expect(fetchFriendsListForSteamUser).toHaveBeenCalledWith(req.steamUserId)
      expect(fetchFriendsListForSteamUser).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(500)
      expect(res.json).toHaveBeenCalledTimes(1)
      expect(res.json).toHaveBeenCalledWith({ error: expectedResponse })

      expect(fetchPlayerSummaryForSteamUsers).not.toHaveBeenCalled()
    })

    it('should friend summary responses into 100 at a time', async () => {
      const req = {
        steamUserId: 'steamUserId'
      }
      const res = {
        json: jest.fn()
      }
      const friendsListResponse = []
      const summariesResponse1 = [{ steamid: 1, personaname: 'taylor' }]
      const summariesResponse2 = [{ steamid: 2, personaname: 'taylor2' }]

      for(let i = 0; i < 101; i++) {
        friendsListResponse.push({ steamid: i })
      }

      const expectedResponse = [{
        steamid: 1,
        username: 'taylor'
      }, {
        steamid: 2,
        username: 'taylor2'
      }]

      fetchFriendsListForSteamUser.mockReturnValueOnce(Promise.resolve(friendsListResponse))
      fetchPlayerSummaryForSteamUsers.mockReturnValueOnce(Promise.resolve(summariesResponse1))
        .mockReturnValueOnce(Promise.resolve(summariesResponse2))

      await handleGetFriends(req, res)

      expect(fetchFriendsListForSteamUser).toHaveBeenCalledWith(req.steamUserId)
      expect(fetchFriendsListForSteamUser).toHaveBeenCalledTimes(1)
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenNthCalledWith(1, friendsListResponse.map(f => f.steamid).slice(0, 100))
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenNthCalledWith(2, friendsListResponse.map(f => f.steamid).slice(100))
      expect(fetchPlayerSummaryForSteamUsers).toHaveBeenCalledTimes(2)
      expect(res.json).toHaveBeenCalledWith(expectedResponse)
    })
  })
})
