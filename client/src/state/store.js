import { configureStore } from '@reduxjs/toolkit'

import rootReducer from './reducers'
import { friendsApi, gamesApi, userApi } from '../services'

const store = configureStore({
  devTools: process.env.NODE_ENV !== 'production',
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(userApi.middleware),
  reducer: {
    ...rootReducer,
    [userApi.reducerPath]: userApi.reducer,
    [friendsApi.reducerPath]: friendsApi.reducer,
    [gamesApi.reducerPath]: gamesApi.reducer
  }
})

export default store