const FRIENDLIST_PRIVATE = 'No friends returned from steam API';
const USER_SUMMARY_PRIVATE = 'No user summary found';

module.exports = {
  FRIENDLIST_PRIVATE,
  USER_SUMMARY_PRIVATE
}