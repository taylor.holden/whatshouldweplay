import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import styled from 'styled-components'
import * as R from 'ramda'

import { Bar, Text } from 'wswpComponents'
import { selectGameSelectorOpen, setGameSelectorOpen, selectSelectedFriendIds, selectUserId } from 'wswpState'

const gamesIntersection = R.innerJoin((game1, game2) => game1.appid === game2.appid)

const getGamesForUserCacheKey = userId => `getGamesForUser("${userId}")`
const selectGamesApi = state => state.gamesApi
const selectGamesListsByFriendId = createSelector(
  selectSelectedFriendIds,
  selectGamesApi,
  selectUserId,
  (friendIds, cache, currentUserId) => {
    const idToGamesMap = friendIds.reduce(
      (idToGamesMap, id) => {
        idToGamesMap[id] = cache.queries[getGamesForUserCacheKey(id)]
        return idToGamesMap
      },
      {}
    ) || {}
    idToGamesMap[currentUserId] = cache.queries[getGamesForUserCacheKey(currentUserId)]

    return idToGamesMap
  }
)
const selectCommonGames = createSelector(
  selectGamesListsByFriendId,
  gameListsByFriendId => Object.keys(gameListsByFriendId)
    .reduce(
      (commonGames, friendId) => {
        if (gameListsByFriendId[friendId] && gameListsByFriendId[friendId].status === 'fulfilled') {
          if (commonGames.length < 1) {
            return commonGames.concat(gameListsByFriendId[friendId].data.games)
          } else {
            return gamesIntersection(gameListsByFriendId[friendId].data.games, commonGames)
          }
        }
        return commonGames
      },
      []
    )
)

const GameSelector = ({ className }) => {
  const dispatch = useDispatch()
  const isOpen = useSelector(selectGameSelectorOpen)
  const games = useSelector(selectCommonGames)

  return (
    <>
      <Bar className={className} onClick={() => dispatch(setGameSelectorOpen(!isOpen))}>
        <Text>Games ({games.length})</Text>
      </Bar>
      <GameContainer open={isOpen}>
        {games.map((game, index) => (
          <Game key={index} selected={game.selected}>
            <img height={32} width={32} src={getGameLogoSrc(game)} /> <Text>{game.name}</Text>
          </Game>
        ))}
      </GameContainer>
    </>
  )
}

const getGameLogoSrc = ({ appid, imgUrl }) => `http://media.steampowered.com/steamcommunity/public/images/apps/${appid}/${imgUrl}.jpg`

const Game = styled.div`
  height: 40px;
  background-color: ${({ theme, selected }) => selected ? 'green' : 'inherit'};
  border-bottom: 1px solid ${({ theme }) => theme.border.color.default};
`
const GameContainer = styled.div`
  overflow: auto;
  height: ${({ open }) => open ? '100%' : 0};
  transition: height 0.3s ease-in-out;
`

export default styled(GameSelector)`
  padding: ${({ theme }) => theme.spacing.unit}px;
`