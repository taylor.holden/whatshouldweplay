import React from 'react'
import styled from 'styled-components'

export default function Card({ children, size, className }) {
  switch(size) {
    case 'small':
      return <SmallCard className={className}>{children}</SmallCard>
    case 'medium':
    default:
      return <DefaultCard className={className}>{children}</DefaultCard>
  }
}

const DefaultCard = styled.div`
  align-items: center;
  align-self: center;
  display: flex;
  flex-direction: column;
  border: 2px solid #171717;
  border-radius: 3px;
  background-color: #223d59;
  box-shadow: 1px 1px #666;
  justify-content: center;
  margin: auto;
  
  height: 300px;
  width: 500px;
`

const SmallCard = styled(DefaultCard)`
  height: 200px;
  width: 200px;
`
