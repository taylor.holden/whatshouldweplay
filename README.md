Table of Contents
- [About](#about)
- [Getting Started](#getting-started)
  - [Running Locally](#running-locally)


## About
WhatShouldWePlay is a node project that allows steam users to find multiplayer games that they share in common with their friends.


## Getting Started

### Running Locally
1. Set up server environment variables:
   1. Create a new file: `./server/config.env`
   2. Add your [Steam API Key](https://steamcommunity.com/dev/apikey) to the new file: `API_KEY=<steam api key>`

2. Install dependencies and start up
```bash
npm ci
npm build
npm start
```