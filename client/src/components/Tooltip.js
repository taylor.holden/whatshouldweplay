import React from 'react'
import styled from 'styled-components'


const Tooltip = ({className, messageText, children}) => {
  return (
    <TooltipContainer className={className}>{children}<TooltipText>{messageText}</TooltipText></TooltipContainer>
  )
}
const TooltipContainer = styled.div`
  display: inline-block;
`

const TooltipText = styled.span`
  visibility: hidden;
  font-size: 0.5em;
  ${TooltipContainer}:hover & {
    visibility: visible;
  }
`
export default Tooltip