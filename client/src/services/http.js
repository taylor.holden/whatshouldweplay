import axios from 'axios'

const BASE_URL = '/'
const TIMEOUT = 1000
const USER_ID_HEADER = 'Steam-User-ID'

export const get = (url, config = {}) => axios({
  method: 'get',
  url: `${BASE_URL}${url}`,
  timeout: TIMEOUT,
  ...config,
  headers: {
    [USER_ID_HEADER]: localStorage.getItem('steamUserId'),
    'Access-Control-Allow-Origin': '*',
    ...config.headers
  },
})
