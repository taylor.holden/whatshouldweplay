import React, { useState } from 'react'
import styled from 'styled-components'

import { Text } from 'wswpComponents'

const UserInfoButton = ({ className }) => {
  const [open, setOpen] = useState(false)

  return <React.Fragment></React.Fragment>
}

export default styled(UserInfoButton)`
  box-shadow: 0 -2px 2px ${({ theme }) => theme.border.color.default};
  padding: 0 ${({ theme }) => theme.spacing.unit}px;
`