import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { Home, Login, NavBar } from './views'

export default function AppRouter() {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route path="/home" component={Home} />
        <Route exact path="/" component={Login} />
      </Switch>
    </Router>
  )
}
