import { createReducer } from '@reduxjs/toolkit'

import { 
  setFriendSelected,
  setFriendSelectorOpen, 
  setGameSelectorOpen 
} from './actions'

const userReducer = createReducer(
  {
    friendSelected: {},
    friendSelectorOpen: true,
    gameSelectorOpen: false
  },
  builder => {
    builder
      .addCase(setFriendSelected, (state, action) => {
        state.friendSelected[action.payload.id] = action.payload.selected
      })
      .addCase(setFriendSelectorOpen, (state, action) => {
        state.friendSelectorOpen = action.payload
        state.gameSelectorOpen = !action.payload
      })
      .addCase(setGameSelectorOpen, (state, action) => {
        state.gameSelectorOpen = action.payload
        state.friendSelectorOpen = !action.payload
      })
  }
)

export default userReducer
