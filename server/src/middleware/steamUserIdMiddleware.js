const USER_ID_HEADER = 'Steam-User-ID'

const steamUserIdMiddleware = (req, res, next) => {
  const steamUserId = getUserId(req)

  if (!steamUserId) {
    res.status(400)
    res.json({ error: 'No Steam User Id provided' })
    return
  }

  req.steamUserId = steamUserId
  next()
}

const getUserId = (req) => req.header(USER_ID_HEADER) || req.header(USER_ID_HEADER.toLowerCase())

module.exports = {
  steamUserIdMiddleware,
  getUserId
}