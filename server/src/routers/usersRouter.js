const express = require('express')
const { fetchGamesForSteamUser, fetchPlayerSummaryForSteamUsers } = require('../services/steamApi')
const errors = require('../constants/errors')

const router = express.Router()

router.get('/', handleGetUser)
router.get('/:userId/games', handleGetGames)

async function handleGetUser(req, res) {
  await fetchPlayerSummaryForSteamUsers([req.steamUserId]).then(userResponse => {
    res.json(userResponse[0])
  }).catch(
    err => {
      if (err === errors.FRIENDLIST_PRIVATE) {
        res.status(400)
      } else {
        res.status(500)
      }

      res.json({ error: err })
    }
  )
}

async function handleGetGames(req, res) {
  try {
    const games = await fetchGamesForSteamUser(req.params.userId)
    games.games = games.games.map(trimGameObject)
    
    res.json(games)
  } catch (err) {
    res.status(500)
    res.json({ error: err })
  }
}

const trimGameObject = ({ appid, img_logo_url, name }) => ({
  appid,
  imgUrl: img_logo_url,
  name
})

module.exports = {
  router,
  handleGetUser,
  handleGetGames
}
