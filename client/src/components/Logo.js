import React from 'react'
import styled from 'styled-components'

import smallTransparent from 'wswpStatic/images/logo_192_transparent.png'
import largeTransparent from 'wswpStatic/images/logo_512_transparent.png'

const Logo = ({ className, height, width, size = 'small', ...otherProps }) => (
  <img className={className} src={size === 'small' ? smallTransparent : largeTransparent} {...otherProps} />
)

export default styled(Logo)``
