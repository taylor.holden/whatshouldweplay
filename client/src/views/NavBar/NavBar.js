import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { isEmpty } from 'ramda'

import { selectUser } from 'wswpState'
import { Bar, Logo, Text } from 'wswpComponents'

import UserInfoButton from './UserInfoButton'

function NavBar({ className }) {
  const loggedInUser = useSelector(selectUser)
  const history = useHistory()

  const handleNotYouClick = () => {
    localStorage.removeItem('steamUserId')
    window.location = '/'
  }

  useEffect(() => {
    if (isEmpty(loggedInUser.userId)) {
      history.push('/')
    }
  }, [loggedInUser, history])

  return (
    <Bar className={className}>
      <Logo style={{ height: 32 }} />
      <Text>What Should We Play?</Text>
      <UserInfoButton />
    </Bar>
  )
}

export default styled(NavBar)`
  align-items: center;
  height: 2em;
`