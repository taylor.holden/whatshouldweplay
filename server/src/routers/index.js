const express = require('express')
const friendsRouter = require('./friendsRouter').router
const usersRouter = require('./usersRouter').router
const { steamUserIdMiddleware } = require('../middleware/steamUserIdMiddleware')

const router = express.Router()
router.use(steamUserIdMiddleware)

router.use('/users', usersRouter)
router.use('/friends', friendsRouter)

module.exports = { router }
