import React from 'react'
import styled from 'styled-components'

const Link = ({ children, className, ...otherProps }) => (
  <a className={className} {...otherProps}>{children}</a>
)

export default styled(Link)`
  color: ${({ theme }) => theme.text.color.link};
`