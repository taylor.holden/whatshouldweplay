import views from './views/reducer'
import user from './user/reducer'

export default {
  user,
  views
}