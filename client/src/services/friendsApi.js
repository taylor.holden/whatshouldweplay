import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { prepareDefaultHeaders } from './apiHelpers'

const friendsApi = createApi({
  reducerPath: 'friendsApi',
  baseQuery: fetchBaseQuery({
    baseUrl: '/api/friends',
    prepareHeaders: prepareDefaultHeaders()
  }),
  endpoints: (builder) => ({
    getFriendsForCurrentUser: builder.query({
      query: () => ''
    })
  })
})

export default friendsApi
