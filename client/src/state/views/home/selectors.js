import { createSelector } from 'reselect'

export const selectFriendIdsToSelectedMap = state => state.views.home.friendSelected
export const selectSelectedFriendIds = createSelector(
  selectFriendIdsToSelectedMap,
  idToSelectedStateMap => Object.keys(idToSelectedStateMap).filter(id => idToSelectedStateMap[id])
)
export const selectNumberOfSelectedFriends = createSelector(
  selectSelectedFriendIds,
  ids => ids.length
)
export const selectFriendSelectorOpen = state => state.views.home.friendSelectorOpen
export const selectGameSelectorOpen = state => state.views.home.gameSelectorOpen
