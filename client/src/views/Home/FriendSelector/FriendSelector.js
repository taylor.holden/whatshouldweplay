import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'
import { is } from 'ramda'

import { Bar, LinearProgressIndicator, Text } from 'wswpComponents'
import { friendsApi } from 'wswpServices'
import { 
  setFriendSelectorOpen, 
  setFriendSelected,
  selectFriendSelectorOpen, 
  selectFriendIdsToSelectedMap,
  selectNumberOfSelectedFriends 
} from 'wswpState'
import MissingFriendsList from '../MissingFriendsList'

import Friend from './Friend'

const isFunction = is(Function)

const FriendSelector = ({ className, onFriendSelected }) => {
  const { data: allFriends, isLoading, error } = friendsApi.useGetFriendsForCurrentUserQuery()
  const selectedFriends = useSelector(selectFriendIdsToSelectedMap)
  const numberOfSelectedFriends = useSelector(selectNumberOfSelectedFriends)
  const isOpen = useSelector(selectFriendSelectorOpen)

  const dispatch = useDispatch()

  const handleFriendClicked = useCallback(friend => {
    if (isFunction(onFriendSelected) && !selectedFriends[friend.steamid]) {
      onFriendSelected(friend)
    }
    dispatch(setFriendSelected({ 
      id: friend.steamid, 
      selected: !selectedFriends[friend.steamid]
    }))
  }, [dispatch, selectedFriends, onFriendSelected])

  return (
    <>
      <Bar onClick={() => dispatch(setFriendSelectorOpen(!isOpen))}>
        <Text>Friends {!isLoading && `(${numberOfSelectedFriends}/${(allFriends || []).length})`}</Text>
        {isLoading && <LinearProgressIndicator />}
      </Bar>
      <FriendsContainer open={isOpen}>
        {!isLoading &&
        error &&
        error.status === 400 && (
          <MissingFriendsList />
        )}
        {!isLoading && allFriends && allFriends.map(friend => (
          <Friend key={friend.steamid} friend={friend} selected={selectedFriends[friend.steamid]} onClick={() => handleFriendClicked(friend)} />
        ))}
      </FriendsContainer>
    </>
  )
}

const FriendsContainer = styled.div`
  overflow: auto;
  transition: height 0.3s ease-in-out;
  height: ${({ open }) => open ? '100%' : 0};
`

export default styled(FriendSelector)`
`